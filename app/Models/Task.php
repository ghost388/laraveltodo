<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Task extends Model
{
    use HasFactory;

    const DRAFT = 'DRAFT';
    const SUBMITTED = 'SUBMITTED';

    protected $state;

    public function __construct(array $attributes = [])
    {
    	parent::__construct($attributes);
    	$this->state = Task::DRAFT;
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getStateAttribute()
    {
    	return $this->state;
    }

    public function submit()
    {
    	$this->submitted_at = Carbon::now();
    	$this->state = Task::SUBMITTED;
    }
}
